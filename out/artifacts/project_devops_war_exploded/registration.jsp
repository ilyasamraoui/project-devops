<%--
  Created by IntelliJ IDEA.
  User: ProBook
  Date: 12/20/2019
  Time: 10:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>>Create an account</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/common.css" rel="stylesheet">
</head>
<body>
<div class="container">

    <form method="POST" action="/signup" class="form-signin">
        <h2 class="form-signin-heading">Create your account</h2>
        <div class="form-group ${(errors.get("usernameLengthError") != null ||  errors.get("usernameExist")!= null)? 'has-error' : ''}">
            <span>${errors.get("usernameLengthError")}</span>
            <span>${errors.get("usernameExist")}</span>
            <div class="form-group">
                <input type="text" name="username" class="form-control" placeholder="Username" autofocus="true">
            </div>
        </div>
        <div class="form-group ${(errors.get("passwordMatchError") != null||  errors.get("passwordLengthError") != null) ? 'has-error' : ''}">
            <span>${errors.get("passwordMatchError")}</span>
            <span>${errors.get("passwordLengthError")}</span>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
            <div class="form-group">
                <input type="password" name="passwordConfirm" class="form-control" placeholder="Confirm your password">
            </div>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
    </form>
</div>
</body>
</html>
