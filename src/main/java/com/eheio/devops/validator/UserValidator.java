package com.eheio.devops.validator;

import com.eheio.devops.model.User;
import com.eheio.devops.service.UserService;

import java.util.HashMap;

public class UserValidator {
    UserService userService;

    public UserValidator() {
        userService = new UserService();
    }

    public boolean valide(User user, HashMap<String, String> errors) {
        boolean result = true;
        if (!validUsernameLength(user.getUsername())) {
            errors.put("usernameLengthError", "username length error");
            result = false;
        }
        if (!validUsername(user.getUsername())) {
            errors.put("usernameExist", "username already exist");
            result = false;
        }
        if (!validPasswordLength(user.getPassword())) {
            errors.put("passwordLengthError", "password length error");
            result = false;
        }
        if (!passowrdMatch(user.getPassword(), user.getPasswordConfirm())) {
            errors.put("passwordMatchError", "paswords do not match");
            result = false;
        }
        return result;
    }

    private boolean validUsernameLength(String username) {
        return username.length() > 4 && username.length() < 32;
    }

    private boolean validPasswordLength(String password) {
        return password.length() > 4 && password.length() < 32;
    }

    private boolean passowrdMatch(String passowrd, String passwordConfirm) {
        return passowrd.equals(passwordConfirm);
    }

    private boolean validUsername(String username) {
        return userService.findByUsername(username) == null;
    }
}
