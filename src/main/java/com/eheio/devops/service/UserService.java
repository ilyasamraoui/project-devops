package com.eheio.devops.service;

import com.eheio.devops.dao.UserDao;
import com.eheio.devops.model.User;

public class UserService {
    private UserDao userDao = new UserDao();

    public boolean login(String username, String password) {
        return userDao.login(username, password);
    }

    public void registration(User user) {
        userDao.register(user);
    }

    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }
}
