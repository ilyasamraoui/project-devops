package com.eheio.devops.dao;

import com.eheio.devops.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserDao {
    private static List<User> users = new ArrayList<User>();
    public UserDao() {
        User user = new User();
        user.setId(0L);
        user.setUsername("admin");
        user.setPassword("admin");
        users.add(user);
    }
    public User findByUsername(String username) {
        for (User user:
        users) {
            if(username.equals(user.getUsername())) {
                return user;
            }
        }
        return null;
    }
    public boolean login(String username, String password) {
        for (User user :
                users) {
            if (username.equals(user.getUsername()) && password.equals(user.getPassword())) {
                return true;
            }
        }
        return false;
       /* Session session = HibernateUtil.getSession();
        if (session != null) {
            try {
                User user = (User) session.get(User.class, username);
                if (password.equals(user.getPassword())) {
                    System.out.println("User: " + user.toString());
                    return true;
                }
            } catch (Exception exception) {
                System.out.println("Exception occred while reading user data: "
                        + exception.getMessage());
                return false;
            }

        } else {
            System.out.println("DB server down.....");
        }
        return false;*/
    }

    public void register(User user) {
        users.add(user);
        /*String msg = "Registration unsuccessful, try again.....";
        Session session = HibernateUtil.getSession();
        if (session != null) {
            try {
                if (user != null) {
                    String username = (String) session.save(user);
                    session.beginTransaction().commit();
                    msg = "User " + username
                            + " created successfully, please login...";
                }
            } catch (Exception exception) {
                System.out.println("Exception occred while reading user data: "
                        + exception.getMessage());
            }

        } else {
            System.out.println("DB server down.....");
        }
        System.out.println("msg:" + msg);
        return msg;*/
    }
}