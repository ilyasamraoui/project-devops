package com.eheio.devops.controller;

import com.eheio.devops.model.User;
import com.eheio.devops.service.UserService;
import com.eheio.devops.validator.UserValidator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

@WebServlet("/signup")
public class SignUp extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = this.getServletContext().getRequestDispatcher("/registration.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserService userService = new UserService();
        UserValidator userValidator = new UserValidator();
        User user = new User();
        user.setUsername(req.getParameter("username"));
        user.setPassword(req.getParameter("password"));
        user.setPasswordConfirm(req.getParameter("passwordConfirm"));
        HashMap<String, String> errors = new HashMap<String, String>();
        if (userValidator.valide(user, errors)) {
            req.getSession().setAttribute("username", user.getUsername());
            userService.registration(user);
            RequestDispatcher requestDispatcher = this.getServletContext().getRequestDispatcher("/");
            requestDispatcher.forward(req, resp);
        } else {
            req.setAttribute("errors", errors);
            RequestDispatcher requestDispatcher = this.getServletContext().getRequestDispatcher("/registration.jsp");
            requestDispatcher.forward(req, resp);
        }

    }
}
