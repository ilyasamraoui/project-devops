package com.eheio.devops.controller;

import com.eheio.devops.model.User;
import com.eheio.devops.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/")
public class Home extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("username") != null) {
            RequestDispatcher requestDispatcher = this.getServletContext().getRequestDispatcher("/welcome.jsp");
            requestDispatcher.forward(req, resp);;
        } else {
            RequestDispatcher requestDispatcher = this.getServletContext().getRequestDispatcher("/login.jsp");
            requestDispatcher.forward(req, resp);
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserService userService = new UserService();
        User user = new User();
        user.setUsername(req.getParameter("username"));
        user.setPassword(req.getParameter("password"));
        if (userService.login(user.getUsername(), user.getPassword())) {
            req.getSession().setAttribute("username",user.getUsername());
            RequestDispatcher requestDispatcher = this.getServletContext().getRequestDispatcher("/welcome.jsp");
            requestDispatcher.forward(req, resp);
            return;
        }
        req.setAttribute("error", "invalid username or password");
        RequestDispatcher requestDispatcher = this.getServletContext().getRequestDispatcher("/login.jsp");
        requestDispatcher.forward(req, resp);
    }
}
