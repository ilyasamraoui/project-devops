package com.eheio.devops.service;

import com.eheio.devops.model.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserServiceTest {
    UserService userService;
    User user;

    @Before
    public void setup() {
        userService = new UserService();
        user = new User();
    }

    @Test
    public void successfulLoginTest() {
        user.setUsername("admin");
        user.setPassword("admin");
        assertTrue(userService.login(user.getUsername(), user.getPassword()));
    }

    @Test
    public void unsuccessfulLoginTest() {
        user.setUsername("sdmlfkj");
        user.setPassword("psodif");
        assertFalse(userService.login(user.getUsername(), user.getPassword()));
    }

    @Test
    public void successfulfindByUsernameTest() {
        assertNotNull(userService.findByUsername("admin"));
    }

    @Test
    public void unsuccessfulfindByUsernameTest() {
        assertNull(userService.findByUsername("qsmldfkj"));
    }

    @After
    public void teardown() {
        userService = null;
        user = null;
    }
}
