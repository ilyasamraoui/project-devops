package com.eheio.devops.validator;

import com.eheio.devops.model.User;
import com.eheio.devops.validator.UserValidator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

public class UserValidatorTest {
    UserValidator userValidator;
    User user;
    @Before
    public void setup() {
        userValidator = new UserValidator();
        user = new User();
    }

    @Test
    public void validUserTest() {
        user.setUsername("user1");
        user.setPassword("user1");
        user.setPasswordConfirm("user1");
        HashMap<String, String> errors = new HashMap<String, String>();
        assertTrue(userValidator.valide(user, errors));
        assertEquals(0,errors.size());
    }

    @Test
    public void unvalidUserNameTest() {
        user.setUsername("admin");
        user.setPassword("admin");
        user.setPasswordConfirm("admin");
        HashMap<String, String> errors = new HashMap<String, String>();
        assertFalse(userValidator.valide(user, errors));
        assertEquals(1,errors.size());
        assertNotNull(errors.get("usernameExist"));
    }

    @After
    public void teardown() {
        userValidator = null;
        user = null;
    }
}
